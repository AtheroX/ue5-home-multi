// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE5HomeMultiGameMode.h"
#include "Character/UE5HomeMultiCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE5HomeMultiGameMode::AUE5HomeMultiGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
