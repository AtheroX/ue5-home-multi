// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Engine/GameInstance.h"
#include "HomeMultiGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class UE5HOMEMULTI_API UHomeMultiGameInstance : public UGameInstance {
	GENERATED_BODY()

public:
	UHomeMultiGameInstance();

protected:
	TSharedPtr<FOnlineSessionSearch> SessionsSearch;

	IOnlineSessionPtr SessionInterface;

	virtual void Init() override;

	virtual void OnCreateSessionComplete(FName SessionName, bool Succeded);
	virtual void OnFindSessionsComplete(bool Succeded);
	virtual void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	UFUNCTION(BlueprintCallable)
	void CreateServer();

	UFUNCTION(BlueprintCallable)
	void JoinServer();

	UFUNCTION(BlueprintCallable)
	void PlayAlone();
};
