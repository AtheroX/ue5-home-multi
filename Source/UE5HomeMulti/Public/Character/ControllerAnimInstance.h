// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Weapons/WeaponBase.h"
#include "ControllerAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class UE5HOMEMULTI_API UControllerAnimInstance : public UAnimInstance {
	GENERATED_BODY()

public:
	UControllerAnimInstance();

protected:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	virtual void NativeBeginPlay() override;
	virtual void WeaponChanged(class AWeaponBase* newWeapon, const class AWeaponBase* oldWeapon);
	virtual void Initialize(const float DeltaTime);
	
public:
	////////////REFS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Anims)
	class AUE5HomeMultiCharacter* myPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Anims)
	class USkeletalMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Anims)
	class AWeaponBase* currentWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Anims)
	FIKProps FikProps;

	////////////IKs
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Anims)
	FTransform cameraTransform;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category=Anims)
	FTransform relativeCameraTransform;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; // Envia replicación de datos
};
