#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UE5HomeMultiCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponChanged, class AWeaponBase*,
                                             newWeapon, const class AWeaponBase*, oldWeapon);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShoot, const FVector, spawnLoc,
                                             const FRotator, spawnRot);

UCLASS(config=Game)
class AUE5HomeMultiCharacter : public ACharacter {
	GENERATED_BODY()

public:
	
	/**
	 * @brief Replicates each variable that is inside this function.
	 * @param OutLifetimeProps All replicated variables.
	 */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/**
	 * @brief Sets the mCurrColor to the chest Material.
	 */
	void SetColor();
	
	/**
	 * @brief @copydoc APawn::TakeDamage.
	 * @return Total damage to take.
	 */
	UFUNCTION(BlueprintCallable, Category=Health)
	virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	

	////////	GETTERS

	/**
	 * @brief Returns the max health of the player as a pure node, so it doesnt has a execution pin.
	 * @return The max health of player.
	 */
	UFUNCTION(BlueprintPure, Category=Health)
	FORCEINLINE float GetMaxHealth() const { return mMaxHealth; }
	/**
	 * @brief Returns the current health of the player as a pure node, so it doesnt has a execution pin.
	 * @return The current health of player.
	 */
	UFUNCTION(BlueprintPure, Category=Health)
	FORCEINLINE float GetCurrentHealth() const { return mCurrHealth; }
	

	////////	SETTERS

	/**
	 * @brief Sets the current health to the aHealthVal.
	 * @param aHealthVal current health to be set.
	 */
	UFUNCTION(BlueprintCallable, Category=Health)
	void SetCurrentHealth(float aHealthVal);

	
	////////	CALLBACKS

	/**
	 * @brief Callback when currHealth changes.
	 */
	UFUNCTION()
	void OnRep_CurrHealth();
	/**
	 * @brief Callback when currHealth changes.
	 */
	UFUNCTION()
	void OnRep_MeshColor();

protected:
	/**
	 * @brief Shows a message depending where its called
	 */
	virtual void OnHealthUpdate();

	/**
	 * @brief If isnt in cooldown tells the server with ServerHandlerFire to shoot a bullet and enters
	 * in cooldown.
	 */
	UFUNCTION(BlueprintCallable, Category="Weapon|Projectile")
	virtual void StartFire();

	/**
	 * @brief Recovers the cooldown of shooting
	 */
	UFUNCTION(BlueprintCallable, Category="Weapon|Projectile")
	virtual void StopFire();

	/**
	 * @brief Checks for authority and equips if server or RPC if client
	 * @param index of the weapon to equip
	 */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	virtual void EquipWeapon(const int32 index);

	/**
	 * @brief 
	 * @param newWeapon 
	 */
	UFUNCTION(Server, Reliable)
	void ServerRPCSetCurrentWeapon(class AWeaponBase* newWeapon);

	UFUNCTION(Server, Reliable) //Sólo ejecuta en server y tiene máxima administración (Equiparable a ServerRPC)
	void ServerHandlerFire();

	UFUNCTION()
	virtual void OnRep_CurrentWeapon(class AWeaponBase* oldWeapon);
	void         OnCurrentWeaponChanges(AWeaponBase* OldWeapon);

public:
	////////	REPLICATED
	UPROPERTY(ReplicatedUsing=OnRep_CurrHealth, VisibleAnywhere, Category = Health)
	float mCurrHealth;									//<! Current health replicated.
	UPROPERTY(ReplicatedUsing=OnRep_meshColor, VisibleAnywhere, BlueprintReadOnly, Category=PlayerVars)
	FLinearColor mMeshColor = FLinearColor::Black;		//<! Color to set the chest of the player.

	////////	DELEGATES
	UPROPERTY(BlueprintAssignable, Category=DELEGATES)
	FOnWeaponChanged evOnWeaponChanged;					//<! Delegate for weapon changes
	UPROPERTY(BlueprintAssignable, Category=DELEGATES)
	FOnShoot evOnShoot;									//<! Delegate for Shooting


	UPROPERTY(EditDefaultsOnly, Category = Health)
	float mMaxHealth;									//<! Max Health of player.
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category="Weapon|Stats")
	int32 mCurrWeaponIndex = 0;							//<! Current index of weapon
	UPROPERTY(ReplicatedUsing=OnRep_CurrentWeapon, VisibleInstanceOnly, BlueprintReadWrite, Category="Weapon|Stats")
	AWeaponBase* mpCurrentWeapon;						//<! Pointer to current weapon, it's replicated
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapon)
	UStaticMeshComponent* weaponMesh;					//<! Pointer to current weapon mesh
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Replicated, Category="Weapon|Stats")
	TArray<AWeaponBase*> mpSpawnedWeapons;				//<! Array to Weapon's pointers 
	UPROPERTY(EditDefaultsOnly, Category=Weapon)
	TArray<TSubclassOf<AWeaponBase>> mDefaultWeapons;	//<! Array of all weapons derivated of WeaponBase
	UPROPERTY(EditDefaultsOnly, Category="Weapon|Projectile")
	TSubclassOf<class ARepliProjectile> mBulletClass;	//<! Blueprint of the bullet that is gonna be shooted
	UPROPERTY(EditDefaultsOnly, Category="Weapon|Projectile")
	float mFireRate;									//<! Time between shoots

	UMaterialInterface* mpColorMaterial;				//<! Pointer to custom material


private:
	//Sin el meta no podemos verlo en blueprint
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	USceneComponent* mpWeaponSocket;					//<! Socket where the weapon is gonna be attached

protected:

	FTimerHandle firingTimer;
	bool         bIsFiringWeapon;

	/////////////////////////


	///////////////////////// ALREADY GENERATED
private:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AUE5HomeMultiCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Input)
	float TurnRateGamepad;

protected:
	void         MoveForward(float Value);
	void         MoveRight(float Value);
	void         TurnAtRate(float Rate);
	void         LookUpAtRate(float Rate);
	virtual void NextWeapon();
	virtual void LastWeapon();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void BeginPlay() override;
	// End of APawn interface


public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};
