// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RepliProjectile.generated.h"

UCLASS()
class UE5HOMEMULTI_API ARepliProjectile : public AActor {
	GENERATED_BODY()

public:
	ARepliProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Comps)
	class USphereComponent* sphereComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Comps)
	class UStaticMeshComponent* staticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Comps)
	class UProjectileMovementComponent* movement;

	UPROPERTY(EditAnywhere, Category=FX)
	UParticleSystem* explosionFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Damage)
	TSubclassOf<class UDamageType> damageType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Damage)
	float damage;

protected:
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	UFUNCTION(Category="Projectile")
	void OnProjectileImpact(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
	                        const FHitResult&    Hit);


public:
	virtual void Tick(float DeltaTime) override;

};
