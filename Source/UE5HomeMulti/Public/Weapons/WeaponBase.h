// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.generated.h"

USTRUCT(BlueprintType)
struct FIKProps {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UAnimSequence* AnimPose;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AimOffset = 15.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform OffsetTransform;
};

UCLASS(Abstract)
class UE5HOMEMULTI_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Comps)
	class USceneComponent* root;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Comps)
	class USkeletalMeshComponent* mesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=State)
	class AUE5HomeMultiCharacter* CurrentOwner;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Config)
	FIKProps FikProps;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Config)
	FTransform WeaponPlacementTransform;
	
	void Shoot(const FVector spawnLoc, const FRotator spawnRot);

	UFUNCTION(BlueprintCallable)
	void AddListener();
	UFUNCTION(BlueprintCallable)
	void RemoveListener();
};
