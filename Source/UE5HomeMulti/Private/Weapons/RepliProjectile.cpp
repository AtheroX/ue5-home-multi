// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/RepliProjectile.h"

#include "Components/SphereComponent.h"
#include "Particles/ParticleSystem.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

ARepliProjectile::ARepliProjectile() {
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true; // Activa la replicación de este objeto

	sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	sphereComponent->InitSphereRadius(30.f);
	sphereComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	RootComponent = sphereComponent;

	//En caso de server añadir la funcionalidad de la bala
	if(GetLocalRole() == ROLE_Authority) {
		sphereComponent->OnComponentHit.AddDynamic(this, &ARepliProjectile::OnProjectileImpact);
	}

	//HardCodeamos el static mesh de sphere
	static ConstructorHelpers::FObjectFinder<UStaticMesh> defaultSphereMesh(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	staticMesh->SetupAttachment(RootComponent);
	if(defaultSphereMesh.Succeeded()) {
		staticMesh->SetStaticMesh(defaultSphereMesh.Object);
		staticMesh->SetRelativeLocation(FVector(0.f, 0.f, -30.f));
		staticMesh->SetRelativeScale3D(FVector(.75f, .75f, .75f));
	}

	//HardCodeamos la partícula de explosión
	static ConstructorHelpers::FObjectFinder<UParticleSystem> defaultExplosionFX(TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion"));
	if(defaultExplosionFX.Succeeded()) { explosionFX = defaultExplosionFX.Object; }

	movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	movement->SetUpdatedComponent(sphereComponent);
	movement->InitialSpeed = 1500.f;
	movement->MaxSpeed = 1500.f;
	movement->bRotationFollowsVelocity = true;
	movement->ProjectileGravityScale = 0.f;

	damageType = UDamageType::StaticClass();
	damage = 10.f;
}

void ARepliProjectile::BeginPlay() { Super::BeginPlay(); }

void ARepliProjectile::Destroyed() {
	Super::Destroyed();
	FVector spawnLoc = GetActorLocation();
	UGameplayStatics::SpawnEmitterAtLocation(this, explosionFX, spawnLoc,
	                                         FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease); //Pool automática de partículas
}

void ARepliProjectile::OnProjectileImpact(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse,
                                          const FHitResult&    Hit) {
	if(OtherActor) {
		UGameplayStatics::ApplyPointDamage(OtherActor, damage, NormalImpulse, Hit, GetInstigator()->Controller, this
		                                   , damageType);
	}

	Destroy();
}


void ARepliProjectile::Tick(float DeltaTime) { Super::Tick(DeltaTime); }
