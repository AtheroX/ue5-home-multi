// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/WeaponBase.h"

#include "Character/UE5HomeMultiCharacter.h"
#include "Weapons/RepliProjectile.h"

// Sets default values
AWeaponBase::AWeaponBase() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	// SetReplicates(true);

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(root);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay() {
	Super::BeginPlay();

	if(!CurrentOwner)
		mesh->SetVisibility(false); //para que sólo se vea en server y se replique 

}

void AWeaponBase::Shoot(const FVector spawnLoc, const FRotator spawnRot) {
	UE_LOG(LogTemp, Warning, TEXT("Shooteando"));

	FActorSpawnParameters spawnParams;
	spawnParams.Instigator = GetInstigator();
	spawnParams.Owner = this;
	
	GetWorld()->SpawnActor<ARepliProjectile>(spawnLoc, spawnRot, spawnParams);

}

void AWeaponBase::AddListener() {
	UE_LOG(LogTemp, Warning, TEXT("Addeando"));

	CurrentOwner->evOnShoot.AddDynamic(this, &AWeaponBase::Shoot);
}
void AWeaponBase::RemoveListener() {
	UE_LOG(LogTemp, Warning, TEXT("DESAddeando"));
	CurrentOwner->evOnShoot.RemoveDynamic(this, &AWeaponBase::Shoot);
}
