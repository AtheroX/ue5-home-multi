// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/UE5HomeMultiCharacter.h"

#include "Weapons/RepliProjectile.h"
#include "Net/UnrealNetwork.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Weapons/WeaponBase.h"

//////////////////////////////////////////////////////////////////////////
// AUE5HomeMultiCharacter
AUE5HomeMultiCharacter::AUE5HomeMultiCharacter() {
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rate for input
	TurnRateGamepad = 50.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;            // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f;       // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)

	// WeaponSocket and Weapon
	mpWeaponSocket = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponSocket"));
	mpWeaponSocket->SetupAttachment(GetMesh());
	weaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	weaponMesh->SetupAttachment(mpWeaponSocket);
	mBulletClass = ARepliProjectile::StaticClass();
	mFireRate = .25f;
	bIsFiringWeapon = false;

	mMaxHealth = 100.0f;
	mCurrHealth = mMaxHealth;

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> sourceMaterial(
		TEXT("/Game/Characters/Mannequins/Materials/Instances/Quinn/MI_Quinn_02.MI_Quinn_02"));
	if(sourceMaterial.Succeeded())
		mpColorMaterial = sourceMaterial.Object;

}


/////////////////////////// REPLICATION
void AUE5HomeMultiCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//HEALTH REPLIC
	DOREPLIFETIME(AUE5HomeMultiCharacter, mCurrHealth);

	DOREPLIFETIME(AUE5HomeMultiCharacter, mMeshColor);

	DOREPLIFETIME(AUE5HomeMultiCharacter, mpSpawnedWeapons);
	DOREPLIFETIME(AUE5HomeMultiCharacter, mpCurrentWeapon);
}

/**
 * @brief
 * Cuando hayamos de actualizar la vida vamos a ir a OnHealthUpdate y allí enviaremos mensajes de vida
 */
void AUE5HomeMultiCharacter::OnRep_CurrHealth() { OnHealthUpdate(); }
/////////////////////////

void AUE5HomeMultiCharacter::BeginPlay() {
	Super::BeginPlay();

	FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, true);
	mpWeaponSocket->AttachToComponent(GetMesh(), rules, TEXT("WeaponSocket"));
	mpWeaponSocket->SetRelativeLocation(FVector(-1.130113f, 1.647979f, 0.731638f));
	mpWeaponSocket->SetRelativeRotation(FRotator(40.000000f, 80.f, -0.000000f));

	if(HasAuthority()) {
		for(const auto& w : mDefaultWeapons) {
			FActorSpawnParameters params;
			params.Owner = this; //Para replicación

			AWeaponBase* spawnWeap = GetWorld()->SpawnActor<AWeaponBase>(w, params);
			const auto   index = mpSpawnedWeapons.Add(spawnWeap);
			if(index == mCurrWeaponIndex) {
				mpCurrentWeapon = spawnWeap;
				OnRep_CurrentWeapon(nullptr);
			}
		}
	}

	if(GetLocalRole() == ROLE_Authority) {
		float r = UKismetMathLibrary::RandomFloat(),
		      g = UKismetMathLibrary::RandomFloat(),
		      b = UKismetMathLibrary::RandomFloat();
		mMeshColor = FLinearColor(r, g, b, 1.f);
		FString crearmsg = FString::Printf(TEXT("Creado el Color %f %f %f - %s"), r, g, b, *GetName());
		GEngine->AddOnScreenDebugMessage(-1, 9999.f, FColor::Blue, crearmsg);
		SetColor();
	}
}


void AUE5HomeMultiCharacter::SetCurrentHealth(float aHealthVal) {
	if(HasAuthority()) {
		// De esta forma forzamos que los updates sólo lo pueda hacer el server, de esa forma nos skippeamos el replicar
		mCurrHealth = FMath::Clamp(aHealthVal, 0.f, mMaxHealth);
		OnHealthUpdate(); // Para actualizar en cliente también
	}
}

float AUE5HomeMultiCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	float dmgApplied = mCurrHealth - Damage;
	SetCurrentHealth(dmgApplied);
	return dmgApplied;
}

void AUE5HomeMultiCharacter::OnRep_MeshColor() { SetColor(); }

void AUE5HomeMultiCharacter::SetColor() {
	if(!mpColorMaterial)
		return;

	FString ponermsg = FString::Printf(TEXT("Poniendo color %f %f %f - %s"), mMeshColor.R, mMeshColor.G, mMeshColor.B, *GetName());
	GEngine->AddOnScreenDebugMessage(-1, 9999.f, FColor::Blue, ponermsg);

	auto newMeshInstance = GetMesh()->CreateDynamicMaterialInstance(1, mpColorMaterial);

	newMeshInstance->SetVectorParameterValue(FName("Tint"), mMeshColor);
}

void AUE5HomeMultiCharacter::OnHealthUpdate() {

	//CLIENT
	if(IsLocallyControlled()) {
		FString healthMsg = FString::Printf(TEXT("%f/%f hp"), mCurrHealth, mMaxHealth);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, healthMsg);

		if(mCurrHealth <= 0) {
			FString deathMsg = FString::Printf(TEXT("DEAD"));
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, deathMsg);
		}
	}
	//SERVER
	if(HasAuthority()) {
		FString healthMsg = FString::Printf(TEXT("%s - %f/%f hp"), *GetFName().ToString(), mCurrHealth, mMaxHealth);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, healthMsg);
	}

}

//////////////////////Weapon
void AUE5HomeMultiCharacter::StartFire() {
	if(!bIsFiringWeapon) {
		bIsFiringWeapon = true;
		UWorld* w = GetWorld();
		w->GetTimerManager().SetTimer(firingTimer, this, &AUE5HomeMultiCharacter::StopFire, mFireRate, false);
		ServerHandlerFire(); // Decimos que estamos disparando y se lo enviamos al servidor
	}
}

void AUE5HomeMultiCharacter::StopFire() { bIsFiringWeapon = false; }

void AUE5HomeMultiCharacter::EquipWeapon(const int32 index) {
	if(!mpSpawnedWeapons.IsValidIndex(index) || mpCurrentWeapon == mpSpawnedWeapons[index])
		return;
	
	if(IsLocallyControlled()) {
		const auto oldw = mpCurrentWeapon;
		// UE_LOG(LogTemp, Warning, TEXT("Cambiando de %s -> %s"), *oldw->GetName(), *currentWeapon->GetName());
		if(GetLocalRole() != ROLE_Authority) {
			// UE_LOG(LogTemp, Warning, TEXT("No Auth, serverRPC"));
			ServerRPCSetCurrentWeapon(mpSpawnedWeapons[index]);
		} else {
			mpCurrentWeapon = mpSpawnedWeapons[index];
			OnRep_CurrentWeapon(oldw);
		}
		mCurrWeaponIndex = index;
	}

}

void AUE5HomeMultiCharacter::OnRep_CurrentWeapon(AWeaponBase* oldWeapon) {
	// UE_LOG(LogTemp, Warning, TEXT("OnRepCurrWeap desde %s"), *GetName());
	OnCurrentWeaponChanges(oldWeapon);
}

void AUE5HomeMultiCharacter::OnCurrentWeaponChanges(AWeaponBase* OldWeapon) {
	if(OldWeapon) {
		OldWeapon->mesh->SetVisibility(false);
		UE_LOG(LogTemp, Warning, TEXT("UNLINK"), *GetName());
		OldWeapon->RemoveListener();
	}

	if(mpCurrentWeapon) {
		if(!mpCurrentWeapon->CurrentOwner) {
			const FTransform& placementTransform = mpCurrentWeapon->WeaponPlacementTransform *
			GetMesh()->GetSocketTransform(FName("WeaponSocket"));

			mpCurrentWeapon->SetActorTransform(placementTransform, false,
			                                 nullptr, ETeleportType::TeleportPhysics);
			mpCurrentWeapon->AttachToComponent(GetMesh(),
			                                 FAttachmentTransformRules::KeepWorldTransform, FName("WeaponSocket"));

			mpCurrentWeapon->CurrentOwner = this;
			UE_LOG(LogTemp, Warning, TEXT("LINK"), *GetName());

		}
		mpCurrentWeapon->AddListener();
		mpCurrentWeapon->mesh->SetVisibility(true);
	}
	evOnWeaponChanged.Broadcast(mpCurrentWeapon, OldWeapon);
}


void AUE5HomeMultiCharacter::ServerRPCSetCurrentWeapon_Implementation(AWeaponBase* newWeapon) {
	const auto oldw = mpCurrentWeapon;
	mpCurrentWeapon = newWeapon;
	// UE_LOG(LogTemp, Warning, TEXT("RPC %s -> %s"), *oldw->GetName(), *newWeapon->GetName());

	OnRep_CurrentWeapon(oldw);
}

/////////////////////////////////////////

/**
 * @brief Esto lo ejecuta únicamente el servidor lo envía a los clientes
 */
void AUE5HomeMultiCharacter::ServerHandlerFire_Implementation() {
	// UE_LOG(LogTemp, Warning, TEXT("RPC de disparo"));

	//Un metro delante y medio metro de altura, (podría modificarse por un Fvector weaponShootPoint)
	FVector  spawnLoc = GetActorLocation() + (GetControlRotation().Vector() * 100.f) + (GetActorUpVector() * 50.f);
	FRotator spawnRot = GetControlRotation();

	UE_LOG(LogTemp, Warning, TEXT("Broadcast"));
	evOnShoot.Broadcast(spawnLoc, spawnRot);
	
	// FActorSpawnParameters spawnParams;
	// spawnParams.Instigator = GetInstigator();
	// spawnParams.Owner = this;
	//
	// GetWorld()->SpawnActor<ARepliProjectile>(spawnLoc, spawnRot, spawnParams);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AUE5HomeMultiCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) {
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AUE5HomeMultiCharacter::StartFire);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AUE5HomeMultiCharacter::NextWeapon);
	PlayerInputComponent->BindAction("LastWeapon", IE_Pressed, this, &AUE5HomeMultiCharacter::LastWeapon);


	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &AUE5HomeMultiCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &AUE5HomeMultiCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("Move Forward / Backward", this, &AUE5HomeMultiCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &AUE5HomeMultiCharacter::MoveRight);


}

void AUE5HomeMultiCharacter::TurnAtRate(float Rate) {
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void AUE5HomeMultiCharacter::LookUpAtRate(float Rate) {
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void AUE5HomeMultiCharacter::MoveForward(float Value) {
	if((Controller != nullptr) && (Value != 0.0f)) {
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AUE5HomeMultiCharacter::MoveRight(float Value) {
	if((Controller != nullptr) && (Value != 0.0f)) {
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AUE5HomeMultiCharacter::NextWeapon() {
	const int32 index = mpSpawnedWeapons.IsValidIndex(mCurrWeaponIndex + 1) ? mCurrWeaponIndex + 1 : 0;
	// UE_LOG(LogTemp, Warning, TEXT("Siguiente Arma %d->%d/%d"), currWeaponIndex, index, spawnedWeapons.Num());
	EquipWeapon(index);
}

void AUE5HomeMultiCharacter::LastWeapon() {
	const int32 index = mpSpawnedWeapons.IsValidIndex(mCurrWeaponIndex - 1) ? mCurrWeaponIndex - 1 : mpSpawnedWeapons.Num() - 1;

	// UE_LOG(LogTemp, Warning, TEXT("Anterior Arma %d->%d/%d"), currWeaponIndex, index, spawnedWeapons.Num());
	EquipWeapon(index);

}
