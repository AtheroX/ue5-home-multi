// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/ControllerAnimInstance.h"

#include "Camera/CameraComponent.h"
#include "Character/UE5HomeMultiCharacter.h"
#include "Net/UnrealNetwork.h"

UControllerAnimInstance::UControllerAnimInstance() {
	
}

void UControllerAnimInstance::NativeBeginPlay() {
	Super::NativeBeginPlay();
	myPlayer = Cast<AUE5HomeMultiCharacter>(TryGetPawnOwner());
	if(myPlayer) {
		mesh = myPlayer->GetMesh();
		myPlayer->evOnWeaponChanged.AddDynamic(this, &UControllerAnimInstance::WeaponChanged);
		WeaponChanged(myPlayer->mpCurrentWeapon, nullptr);
	}
}

void UControllerAnimInstance::NativeUpdateAnimation(float DeltaSeconds) {
	Super::NativeUpdateAnimation(DeltaSeconds);

	if(!myPlayer) return;

	Initialize(DeltaSeconds);
}

void UControllerAnimInstance::WeaponChanged(AWeaponBase* newWeapon, const AWeaponBase* oldWeapon) {
	currentWeapon = newWeapon;
	if(currentWeapon) {
		FikProps = currentWeapon->FikProps;
	}
}

void UControllerAnimInstance::Initialize(const float DeltaTime) {
	//BaseAimRot está replicado, si fuese la rotación de la cámara no funcionaría
	cameraTransform = FTransform(myPlayer->GetBaseAimRotation(),
		myPlayer->GetFollowCamera()->GetComponentLocation());

	const FTransform& RootOffset = mesh->GetSocketTransform(FName("root"), RTS_Component).Inverse() *
		mesh->GetSocketTransform(FName("ik_hand_root"));
	relativeCameraTransform = cameraTransform.GetRelativeTransform(RootOffset);
}


void UControllerAnimInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(UControllerAnimInstance, relativeCameraTransform);
	
}
