// Fill out your copyright notice in the Description page of Project Settings.


#include "HomeMultiGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

UHomeMultiGameInstance::UHomeMultiGameInstance() { }

void UHomeMultiGameInstance::Init() {
	Super::Init();
	if(IOnlineSubsystem* sub = IOnlineSubsystem::Get()) {
		SessionInterface = sub->GetSessionInterface();
		if(SessionInterface.IsValid()) {
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UHomeMultiGameInstance::OnCreateSessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UHomeMultiGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UHomeMultiGameInstance::OnJoinSessionComplete);
		}
	}
}

void UHomeMultiGameInstance::OnCreateSessionComplete(FName SessionName, bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("CreateServer succeded"));
		GetWorld()->ServerTravel("/Game/ThirdPerson/Maps/ThirdPersonMap?listen"); //?listen es una sessión de escucha al servidor
	}	
}

void UHomeMultiGameInstance::OnFindSessionsComplete(bool Succeded) {
	if(Succeded) {
		UE_LOG(LogTemp, Warning, TEXT("Search succeded"));
		TArray<FOnlineSessionSearchResult> searchResults = SessionsSearch->SearchResults;

		if(searchResults.Num()) { // 0 = false
			SessionInterface->JoinSession(0, FName("HomeMulti Session"), searchResults[0]); //HardCode de conexión al primer server que exista
		}
	}
}

void UHomeMultiGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {

	UE_LOG(LogTemp, Warning, TEXT("Joined succeded"));

	// El 0 porque ahora mismo somos los únicos jugadores ya que no estamos en un server
	if(APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0)) {
		FString joinAddress = "";
		SessionInterface->GetResolvedConnectString(SessionName, joinAddress);
		if(joinAddress != "") {
			PlayerController->ClientTravel(joinAddress, ETravelType::TRAVEL_Absolute); // Mover player a otro server
		}
	}
}

void UHomeMultiGameInstance::CreateServer() {

	UE_LOG(LogTemp, Warning, TEXT("CreateServer"));
	FOnlineSessionSettings settings;
	settings.bAllowJoinInProgress = true;
	settings.bIsDedicated = false;
	settings.bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	settings.bShouldAdvertise = true;
	settings.bUsesPresence = true;
	settings.NumPublicConnections = 5;

	SessionInterface->CreateSession(0, FName("HomeMulti Session"), settings);
}

void UHomeMultiGameInstance::JoinServer() {

	UE_LOG(LogTemp, Warning, TEXT("JoinServer"));

	SessionsSearch = MakeShareable(new FOnlineSessionSearch());
	SessionsSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	SessionsSearch->MaxSearchResults = 10000;
	SessionsSearch->QuerySettings.Set("SEARCH_PRESENCE", true, EOnlineComparisonOp::Equals);

	SessionInterface->FindSessions(0, SessionsSearch.ToSharedRef());
}

void UHomeMultiGameInstance::PlayAlone() {
	UE_LOG(LogTemp, Warning, TEXT("PlayAlone"));
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/ThirdPerson/Maps/ThirdPersonMap", true);
	// GetWorld()->("/Game/ThirdPerson/Maps/ThirdPersonMap?listen"); //?listen es una sessión de escucha al servidor

}
